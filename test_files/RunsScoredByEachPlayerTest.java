import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RunsScoredByEachPlayerTest {
    @Test
    public void Equals() {
        RunsScoredByEachPlayer obj = new RunsScoredByEachPlayer();
        String expectedOutput = "{DA Warner=14, S Dhawan=40, MC Henriques=52, Yuvraj Singh=62, DJ Hooda=16, BCJ Cutting=16, CH Gayle=32, Mandeep Singh=24, TM Head=30, KM Jadhav=31, SR Watson=22, Sachin Baby=1, STR Binny=11, S Aravind=0, YS Chahal=3, TS Mills=6, A Choudhary=6}";
        assertEquals(expectedOutput, obj.main().toString());
    }

    @Test
    public void NotNull() {
        RunsScoredByEachPlayer obj = new RunsScoredByEachPlayer();
        String expectedOutput = "{DA Warner=14, S Dhawan=40, MC Henriques=52, Yuvraj Singh=62, DJ Hooda=16, BCJ Cutting=16, CH Gayle=32, Mandeep Singh=24, TM Head=30, KM Jadhav=31, SR Watson=22, Sachin Baby=1, STR Binny=11, S Aravind=0, YS Chahal=3, TS Mills=6, A Choudhary=6}";
        assertNotNull(obj.main().toString(), "null");
    }

    @Test
    public void NotEquals() {
        RunsScoredByEachPlayer obj = new RunsScoredByEachPlayer();
        String expectedOutput = "{DDA Warner=14, S Dhawan=40, MC Henriques=52, Yuvraj Singh=62, DJ Hooda=16, BCJ Cutting=16, CH Gayle=32, Mandeep Singh=24, TM Head=30, KM Jadhav=31, SR Watson=22, Sachin Baby=1, STR Binny=11, S Aravind=0, YS Chahal=3, TS Mills=6, A Choudhary=6}";
        assertNotEquals(expectedOutput, obj.main().toString());
    }
}