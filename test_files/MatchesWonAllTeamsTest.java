import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatchesWonAllTeamsTest {
    @Test
    public void Equals() {
        MatchesWonAllTeams obj = new MatchesWonAllTeams();
        String expectedOutput = "{Mumbai Indians=92, Sunrisers Hyderabad=42, Pune Warriors=12, Rajasthan Royals=63, Kolkata Knight Riders=77, Royal Challengers Bangalore=73, Gujarat Lions=13, Rising Pune Supergiant=10, Kochi Tuskers Kerala=6, Kings XI Punjab=70, Deccan Chargers=29, Delhi Daredevils=62, Rising Pune Supergiants=5, Chennai Super Kings=79}";
        assertEquals(expectedOutput, obj.main().toString());
    }

    @Test
    public void NotNull() {
        MatchesWonAllTeams obj = new MatchesWonAllTeams();
        String expectedOutput = "{Mumbai Indians=92, Sunrisers Hyderabad=42, Pune Warriors=12, Rajasthan Royals=63, Kolkata Knight Riders=77, Royal Challengers Bangalore=73, Gujarat Lions=13, Rising Pune Supergiant=10, Kochi Tuskers Kerala=6, Kings XI Punjab=70, Deccan Chargers=29, Delhi Daredevils=62, Rising Pune Supergiants=5, Chennai Super Kings=79}";
        assertNotNull(obj.main().toString());
    }

    @Test
    public void NotEquals() {
        MatchesWonAllTeams obj = new MatchesWonAllTeams();
        String expectedOutput = "{Mumbai Indians=922, Sunrisers Hyderabad=42, Pune Warriors=12, Rajasthan Royals=63, Kolkata Knight Riders=77, Royal Challengers Bangalore=73, Gujarat Lions=13, Rising Pune Supergiant=10, Kochi Tuskers Kerala=6, Kings XI Punjab=70, Deccan Chargers=29, Delhi Daredevils=62, Rising Pune Supergiants=5, Chennai Super Kings=79}";
        assertNotEquals(expectedOutput, obj.main().toString());
    }

}