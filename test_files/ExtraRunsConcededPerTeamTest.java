import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

public class ExtraRunsConcededPerTeamTest {
    @Test
    public void Equals() {
        ExtraRunsConcededPerTeam obj = new ExtraRunsConcededPerTeam();
        String expectedOutput = "{Gujarat Lions=98, Mumbai Indians=102, Sunrisers Hyderabad=107, Kings XI Punjab=100, Delhi Daredevils=106, Rising Pune Supergiants=108, Kolkata Knight Riders=122, Royal Challengers Bangalore=156}";
        assertEquals(expectedOutput, obj.main().toString());
    }

    @Test
    public void NotNull() {
        ExtraRunsConcededPerTeam obj = new ExtraRunsConcededPerTeam();
        String expectedOutput = "{Gujarat Lions=98, Mumbai Indians=102, Sunrisers Hyderabad=107, Kings XI Punjab=100, Delhi Daredevils=106, Rising Pune Supergiants=108, Kolkata Knight Riders=122, Royal Challengers Bangalore=156}";
        assertNotNull(obj.main().toString(), "null");
    }

    @Test
    public void NotEquals() {
        ExtraRunsConcededPerTeam obj = new ExtraRunsConcededPerTeam();
        String expectedOutput = "{Gujarat Lions=988, Mumbai Indians=102, Sunrisers Hyderabad=107, Kings XI Punjab=100, Delhi Daredevils=106, Rising Pune Supergiants=108, Kolkata Knight Riders=122, Royal Challengers Bangalore=156}";
        assertNotEquals(expectedOutput, obj.main().toString());
    }
}