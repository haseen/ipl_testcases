import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runner.*;

@RunWith(Suite.class)
@SuiteClasses({
        MatchesPlayedPerYearTest.class,
        MatchesWonAllTeamsTest.class,
        RunsScoredByEachPlayerTest.class,
        ExtraRunsConcededPerTeamTest.class,
        TopEconomicalBowlersTest.class
})

public class JUnitTestSuite {
    @BeforeClass
    public static void printMe() {
        System.out.println("Running");

    }
}