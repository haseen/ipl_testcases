import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TopEconomicalBowlersTest {
    @Test
    public void Equals() {
        TopEconomicalBowlers obj = new TopEconomicalBowlers();
        String expectedOutput = "{RN ten Doeschate=4.0, J Yadav=4.142857, V Kohli=5.0, R Ashwin=5.8717947, S Nadeem=6.142857, Parvez Rasool=6.2, MC Henriques=6.56, Z Khan=6.625, M Vijay=7.0, GB Hogg=7.047619}";
        assertEquals(expectedOutput, obj.main().toString());
    }

    @Test
    public void NotNull() {
        TopEconomicalBowlers obj = new TopEconomicalBowlers();
        String expectedOutput = "{RN ten Doeschate=4.0, J Yadav=4.142857, V Kohli=5.0, R Ashwin=5.8717947, S Nadeem=6.142857, Parvez Rasool=6.2, MC Henriques=6.56, Z Khan=6.625, M Vijay=7.0, GB Hogg=7.047619}";
        assertNotNull(obj.main().toString());
    }

    @Test
    public void NotEquals() {
        TopEconomicalBowlers obj = new TopEconomicalBowlers();
        String expectedOutput = "{RN ten Doeschate=14.0, J Yadav=4.142857, V Kohli=5.0, R Ashwin=5.8717947, S Nadeem=6.142857, Parvez Rasool=6.2, MC Henriques=6.56, Z Khan=6.625, M Vijay=7.0, GB Hogg=7.047619}";
        assertNotEquals(expectedOutput, obj.main().toString());
    }
}