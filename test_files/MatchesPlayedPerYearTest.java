import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

public class MatchesPlayedPerYearTest {
    @Test
    public void Equals() {
        MatchesPlayedPerYear obj = new MatchesPlayedPerYear();
        String expectedOutput = "{2009=57, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, 2011=73, 2010=60}";
        assertEquals(expectedOutput, obj.main().toString());
    }

    @Test
    public void NotNull() {
        MatchesPlayedPerYear obj = new MatchesPlayedPerYear();
        String expectedOutput = "{2009=57, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, 2011=73, 2010=60}";
        assertNotNull(obj.main().toString());
    }

    @Test
    public void NotEquals() {
        MatchesPlayedPerYear obj = new MatchesPlayedPerYear();
        String expectedOutput = "{2009=597, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, 2011=73, 2010=60}";
        assertNotEquals(expectedOutput, obj.main().toString());
    }
}